import uuid from 'uuid';

export class Model {
  items: ITodoItem[];

  constructor() {
    this.items = [
      new TodoItem(uuid.v4(), 'Buy Flowers', false),
      new TodoItem(uuid.v4(), 'Get Shoes', false),
      new TodoItem(uuid.v4(), 'Collect Tickets', false),
      new TodoItem(uuid.v4(), 'Call Joe', false)
    ];
  }

}

interface ITodoItem {
  id: number;
  text: string;
  isDone: boolean;
}

export class TodoItem {
  id: number;
  text: string;
  isDone: boolean;

  constructor(id, text, isDone) {
    this.id = id;
    this.text = text;
    this.isDone = isDone;
  }
}
