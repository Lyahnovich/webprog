import { Component } from '@angular/core';
import uuid from 'uuid';
import {Model, TodoItem} from './model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'TodoApp';

  model = new Model();

  getTodoItems = () => this.model.items;

  addItem = (newItem) => !!newItem && this.model.items.push(new TodoItem(uuid.v4(), newItem, false));

  deleteItem = (id) => {
    this.model.items = this.model.items.filter((item => item.id !== id));
  }
}
