## Личная информация:  
**ФИ**: Ляхнович Иван  
**Группа**: ВТиП-302
___
**Роль**: 🎮 Gamer 👾   
**Язык программирования**: 💻 JavaScript 🌐   
**Программы**: ⌨️ Visual Studio Code 🖥
___
**Сертификат**: https://www.intuit.ru/verifydiplomas/101308943  
**Курсовая работа**: https://lyahnovich.gitlab.io/lyahnovich/coursework/  
**Готовая игра для курсовой работы**: https://ivan-lyahnovich.github.io/piggy-man.html?07